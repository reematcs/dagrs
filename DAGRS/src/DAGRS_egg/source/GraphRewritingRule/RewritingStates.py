import RewritingState as re_state
import RuleMap as rm
from PyQt5.QtSvg import QSvgRenderer, QGraphicsSvgItem
from PyQt5.QtCore import (QPoint, qAbs, QLineF, QPointF,
                          qrand, QRectF, QSizeF, QSize, qsrand,
                          Qt, QTime)
from PyQt5.QtGui import QImage, QImageWriter, qRgb
from PyQt5.QtGui import (QBrush, QColor, QLinearGradient, QPainter,
                         QPainterPath, QPen, QPolygonF, QRadialGradient)
from PyQt5.QtWidgets import (QApplication, QGraphicsItem, QGraphicsScene,
                             QGraphicsView, QStyle, QDesktopWidget)
import math
import Queue as q


class RewritingStates(object):
    def __init__(self, graphwidget, copy_mem={}, state_0=None, states=None):
        self.copy_mem = copy_mem
        self.graphwidget = graphwidget
        self.states = states
        self.state_0 = state_0
        self.current_state = self.state_0
        self.rule_map = rm.RuleMap()
        RewritingStates.state_count = 0

    def new_rewriting_state(self, state_graph):
        return re_state.RewritingState(state_graph)
    
    def get_next_state(self, rule=0, subgraph=0):
        state = self.current_state
        self.current_state =\
            self.current_state.get_next_state(rule,
                                              subgraph)
        return state

    def get_next_states(self):
        states = self.current_state.get_next_states()
        self.current_state =\
            self.current_state.get_next_state()            
        return states

    def add_states_to_current_state(self, states, 
                                    subgraphs, rules):
        self.current_state.add_next_states(states, subgraphs, rules)

    def add_states(self, state_i, states, subgraphs, rules):
        state_i.add_next_states(states, subgraphs, rules)

    def explore_all_states(self):
        self.state_0.print_state()
        self.state_0.apply_rules(self.rule_map)
        exploration_states = self.state_0.get_next_states()
        
        exploration_queue = q.Queue()
        explored_queue = q.Queue()
        
        for rule in exploration_states:
            for subgraph in exploration_states[rule]:
                exploration_queue.put(exploration_states[rule][subgraph])
        
        #while not exploration_queue.empty():
        state = exploration_queue.get()
        explored_queue.put(state)
        state.apply_rules(self.rule_map)
        state.print_state()
        next_states = state.get_next_states()
        for rule in next_states:
            for subgraph in next_states[rule]:
                print(explored_queue.queue)
                if next_states[rule][subgraph] in explored_queue.queue:
                    next_states[rule].pop(subgraph)
                    break
                exploration_queue.put(next_states[rule][subgraph])

    def draw_all_states(self, scene):
        sceneRect = scene.sceneRect()
        width = sceneRect.width()
        height = sceneRect.height()
        exploration_states = self.state_0.get_next_states()
        explored_queue = q.Queue()
        explored_queue.put(self.state_0)
        exploration_queue = q.Queue()
        scene.addItem(self.state_0)
        init_x = -width
        init_y =  -height/2
        self.state_0.setPos(init_x, init_y)
        x_dist = 100
        y_dist = 100
        new_x = init_x
        new_y = -height
        for rule in exploration_states:
            for subgraph in exploration_states[rule]:
                exploration_queue.put(exploration_states[rule][subgraph]) 
                new_y = new_y + y_dist
                scene.addItem(exploration_states[rule][subgraph])
                scene.addItem(Edge(self.state_0, 
                              exploration_states[rule][subgraph]))
                exploration_states[rule][subgraph].setPos(new_x, new_y)
        print(exploration_queue.queue)
        
        while not exploration_queue.empty():
            new_x = new_x + x_dist
            new_y = -height            
            current_state = exploration_queue.get()
            explored_queue.put(current_state)
            next_states = current_state.get_next_states()
            for rule in next_states:
                for subgraph in next_states[rule].keys():
                    new_y = new_y + y_dist                    
                    if next_states[rule][subgraph] in explored_queue.queue:
                        next_states[rule].pop(subgraph)
                        continue
                    scene.addItem(next_states[rule][subgraph])
                    scene.addItem(Edge(current_state, 
                                       next_states[rule][subgraph]))
                    next_states[rule][subgraph].setPos(new_x, new_y)
    

class Edge(QGraphicsItem):
    Pi = math.pi
    TwoPi = 2.0 * Pi

    Type = QGraphicsItem.UserType + 2

    def __init__(self, sourceNode, destNode):
        super(Edge, self).__init__()

        self.arrowSize = 10.0
        self.sourcePoint = QPointF()
        self.destPoint = QPointF()

        self.setAcceptedMouseButtons(Qt.NoButton)
        self.source = sourceNode
        self.dest = destNode
        self.source.addEdge(self)
        self.dest.addEdge(self)
        self.adjust()

    def type(self):
        return Edge.Type

    def sourceNode(self):
        return self.source

    def setSourceNode(self, node):
        self.source = node
        self.adjust()

    def destNode(self):
        return self.dest

    def setDestNode(self, node):
        self.dest = node
        self.adjust()

    def adjust(self):
        if not self.source or not self.dest:
            return

        line = QLineF(self.mapFromItem(self.source, 0, 0),
                      self.mapFromItem(self.dest, 0, 0))
        length = line.length()

        self.prepareGeometryChange()

        if length > 50.0:
            edgeOffset = QPointF((line.dx() * 55) / length,
                                (line.dy() * 55) / length)

            self.sourcePoint = line.p1() + edgeOffset
            self.destPoint = line.p2() - edgeOffset
        else:
            self.sourcePoint = line.p1()
            self.destPoint = line.p1()

    def boundingRect(self):
        if not self.source or not self.dest:
            return QRectF()

        penWidth = 1.0
        extra = (penWidth + self.arrowSize) / 2.0

        return QRectF(self.sourcePoint,
                      QSizeF(self.destPoint.x() - self.sourcePoint.x(),
                             self.destPoint.y() - self.sourcePoint.y())
                      ).normalized().adjusted(-extra, -extra, extra, extra)

    def paint(self, painter, option, widget):
        if not self.source or not self.dest:
            return

    # Draw the line itself.
        line = QLineF(self.sourcePoint, self.destPoint)

        if line.length() == 0.0:
            return

        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine, Qt.RoundCap,
                       Qt.RoundJoin))
        painter.drawLine(line)

        # Draw the arrows if there's enough room.
        angle = math.acos(line.dx() / line.length())
        if line.dy() >= 0:
            angle = Edge.TwoPi - angle

        sourceArrowP1 = self.sourcePoint +\
            QPointF(math.sin(angle + Edge.Pi / 3) * self.arrowSize,
                    math.cos(angle + Edge.Pi / 3) * self.arrowSize)
        sourceArrowP2 = self.sourcePoint +\
            QPointF(math.sin(angle + Edge.Pi - Edge.Pi / 3) * self.arrowSize,
                    math.cos(angle + Edge.Pi - Edge.Pi / 3) * self.arrowSize)
        destArrowP1 = self.destPoint +\
            QPointF(math.sin(angle - Edge.Pi / 3) * self.arrowSize,
                    math.cos(angle - Edge.Pi / 3) * self.arrowSize)
        destArrowP2 = self.destPoint +\
            QPointF(math.sin(angle - Edge.Pi + Edge.Pi / 3) * self.arrowSize,
                    math.cos(angle - Edge.Pi + Edge.Pi / 3) * self.arrowSize)

        painter.setBrush(Qt.black)
        painter.drawPolygon(QPolygonF([line.p1(),
                                       sourceArrowP1, sourceArrowP2]))
        #painter.drawPolygon(QPolygonF([line.p2(), destArrowP1, destArrowP2]))
    