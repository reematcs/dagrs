import graph_tool.all as gt
import copy
###TODO get_property(vertex), set_property(vertex)
###TODO add_property(name, values), delete_property(name)
import collections as col
from subprocess import call
import __builtin__

home_dir = "/Users/reemmokhtar/Dropbox/Documents/CS Department" +\
           "/Research/graph_grammars/DNAgrs/code/GraphRewritingSystem" +\
           "/DAGRS/src/DAGRS_egg"

__builtin__.home_dir = home_dir


class RuleGraph(object):
    hybridizedVertexProperties =\
        dict([('fill_color', "black"), ("anchor", 1)])
    unhybridizedVertexProperties =\
        dict([('fill_color', "white"), ("anchor", 1)])

    covalentEdgeProperties =\
        dict([('mid_marker', "none"),
            ('end_marker', "arrow"),
            ('mid_marker_pos', 0.5),
            ('dash_style', [])])

    hybridizationEdgeProperties =\
        dict([('mid_marker', "none"),
            ('end_marker', "none"),
            ('mid_marker_pos', 0.5),
            ('dash_style', [0.05, 0.05, 0.05, 0.05])])

    strandEndEdgeProperties =\
        dict([('mid_marker', "bar"),
            ('end_marker', "bar"),
            ('mid_marker_pos', 0.5),
            ('dash_style', [])])

    vertex_drawing_properties =\
        dict([("hybridized", hybridizedVertexProperties),
            ("unhybridized", unhybridizedVertexProperties)])

    edge_drawing_properties =\
        dict([("hybridization", hybridizationEdgeProperties),
            ("covalent", covalentEdgeProperties),
            ("strandend", strandEndEdgeProperties)])

    # covalentEdgeProperties =\
    #     dict([('mid_marker', "circle"),
    #         ('end_marker', "square"),
    #         ('mid_marker_pos', 0.5),
    #         ('dash_style', [])])

    # hybridizationEdgeProperties =\
    #     dict([('mid_marker', "circle"),
    #         ('end_marker', "circle"),
    #         ('mid_marker_pos', 0.5),
    #         ('dash_style', [0.05, 0.05, 0.05, 0.05])])

    # strandEndEdgeProperties =\
    #     dict([('mid_marker', "bar"),
    #         ('end_marker', "bar"),
    #         ('mid_marker_pos', 0.5),
    #         ('dash_style', [])])
    # vertex_drawing_properties =\
    #     dict([("hybridized", hybridizedVertexProperties),
    #         ("unhybridized", unhybridizedVertexProperties)])

    # edge_drawing_properties =\
    #     dict([("hybridization", hybridizationEdgeProperties),
    #         ("covalent", covalentEdgeProperties),
    #         ("strandend", strandEndEdgeProperties)])

    vertex_prop_keys = ['domain', 'state']
    edge_prop_keys = ['bond']

    draw_vprop_keys = ['fill_color']
    draw_eprop_keys = ['mid_marker', 'mid_marker_pos',
                       'end_marker', 'dash_style']

    draw_vprop_types = {'fill_color': "string"}
    draw_eprop_types = {'mid_marker': "string", 'mid_marker_pos': "float",
                        'end_marker': "string", 'dash_style': "object"}

    def __init__(self, name="none", rule_graph=gt.Graph()):
        self.name = name
        # specifies the rule side graph
        self.set_graph(rule_graph)
        # specifies the graph vertex properties
        self.set_vertex_property_maps(rule_graph.vertex_properties)
        # specifies the graph edge properties
        self.set_edge_property_maps(rule_graph.edge_properties)
        self.vertex_mask = {}
        self.edge_mask = {}
        self.edge_is_suppressed = None

    def __eq__(self, other):
        result = gt.isomorphism(self.get_graph(), other.get_graph())
        return result

    def set_vertex_filter(self, vmask):
        self.vertex_mask = vmask
        self.get_graph().set_vertex_filter(vmask)

    def set_edge_filter(self, emask):
        self.edge_mask = emask
        self.get_graph().set_edge_filter(emask)

    def get_vertex_filter(self):
        return self.vertex_mask

    def get_edge_filter(self):
        return self.edge_mask

    def set_graph(self, rule_graph):
        self.RG = rule_graph

    def set_name(self, name):
        self.name = name

    def get_name(self):
        return self.name

    def set_vertex_property_map_value(self, name, vertex, value):
        self.get_graph().vertex_properties[name][vertex] = value

    def get_vertex_property_map_value(self, name, vertex):
        if not isinstance(vertex, gt.Vertex):
            v = self.get_graph().vertex(vertex)
            return self.get_graph().vertex_properties[name][v]
        return self.get_graph().vertex_properties[name][vertex]

    def set_vertex_property_map(self, name, values):
        self.get_graph().vertex_properties[name] = values

    def set_vertex_property_maps(self, rule_property_maps={}):
        for prop in RuleGraph.draw_vprop_keys:
            if prop not in rule_property_maps.keys():
                new_prop =\
                    self.get_graph().new_vertex_property(
                        RuleGraph.draw_vprop_types[prop])
                self.get_graph().vertex_properties[prop] = new_prop

        for v in self.get_graph().vertices():
            for prop in RuleGraph.draw_vprop_keys:
                self.get_graph().vertex_properties[prop][v] =\
                    RuleGraph.vertex_drawing_properties[
                        rule_property_maps["state"][v]][prop]

    def get_vertex_property_maps(self):
        return self.get_graph().vertex_properties

    def get_vertex_property_map(self, property_name):
        return self.get_graph().vertex_properties[property_name]

    def get_vertex_property_map_keys(self):
        return self.get_graph().vertex_properties.keys()

    def set_edge_property_map_value(self, name, edge, value):
        self.get_graph().edge_properties[name][edge] = value

    def get_edge_property_map_value(self, name, edge):
        return self.get_graph().edge_properties[name][edge]

    def set_edge_property_map(self, name, values):
        self.get_graph().edge_properties[name] = values

    def set_edge_property_maps(self, rule_property_maps):
        if 'bond' not in self.get_graph().edge_properties.keys():
            new_prop =\
                self.get_graph().new_edge_property(
                    "string")
            self.get_graph().edge_properties['bond'] = new_prop
        for prop in RuleGraph.draw_eprop_keys:
            if prop not in rule_property_maps.keys():
                new_prop =\
                    self.get_graph().new_edge_property(
                        RuleGraph.draw_eprop_types[prop])
                self.get_graph().edge_properties[prop] = new_prop
        for e in self.get_graph().edges():
            self.get_graph().edge_properties['bond'][e] =\
                rule_property_maps['bond'][e]
            for prop in RuleGraph.draw_eprop_keys:
                self.get_graph().edge_properties[prop][e] =\
                    RuleGraph.edge_drawing_properties[
                        rule_property_maps['bond'][e]][prop]

    def get_edge_property_maps(self):
        return self.get_graph().edge_properties

    def get_edge_property_map(self, property_name):
        return self.get_graph().edge_properties[property_name]

    def get_edge_property_map_keys(self):
        return self.get_graph().edge_properties.keys()

    def get_graph(self):
        return self.RG

    def __str__(self):
        output = "name:\t"+self.get_name()+"\ngraph:\t"+str(self.get_graph())
        for v in self.get_graph().vertices():
            for prop in RuleGraph.draw_vprop_keys:
                output = output + str(prop) +\
                    str(self.get_vertex_property_map_value(prop, v))
        return output

    def print_graph(self, suffix="next"):
        edge_is_suppressed = self.suppress_hybridization_edges()

        self.RG.set_edge_filter(edge_is_suppressed)
        print("prefix\t"+suffix)
        printed_graph_path = str(home_dir +
                                 "/examples/states/" + str(self.name) +
                                 "_" + str(suffix) + str("_graph-draw.svg"))

        printed_graph_path_fixed = str(home_dir +
                                       "/examples/states/" + str(self.name) +
                                       "_" + str(suffix) +
                                       str("_cairosvg.svg"))

        pos = gt.sfdp_layout(self.RG, C=4.0, K=1.0)

        gt.graph_draw(self.RG, pos=pos,
                      vertex_fill_color=
                      self.get_vertex_property_map('fill_color'),
                      vertex_anchor=1,
                      vertex_text=
                      self.get_vertex_property_map('domain'),
                      vertex_font_size=18,
                      eprops={"color": "black",
                              "pen_width": 5.5},
                      edge_mid_marker_pos=
                      self.get_edge_property_map(
                          'mid_marker_pos'),
                      edge_mid_marker=
                      self.get_edge_property_map('mid_marker'),
                      edge_end_marker=
                      self.get_edge_property_map('end_marker'),
                      edge_dash_style=
                      self.get_edge_property_map('dash_style'),
                      output=printed_graph_path, output_size=(400,400))

        self.RG.set_edge_filter(None)

        call(["cairosvg", printed_graph_path,
              "-o", printed_graph_path_fixed])
        return printed_graph_path_fixed

    def suppress_hybridization_edges(self):
        suppressed_hybridization_edges = []

        for edge in self.RG.edges():
            if self.RG.edge_properties['bond'][edge] == "hybridization":
                suppressed_hybridization_edges.append(edge)

        for edge in suppressed_hybridization_edges:
            for edge2 in suppressed_hybridization_edges:
                if edge.source() == edge2.target() and\
                   edge.target() == edge2.source():
                    suppressed_hybridization_edges.remove(edge2)
                    break

        edge_is_suppressed = self.RG.new_edge_property("bool")
        for edge in self.RG.edges():
            if edge in suppressed_hybridization_edges:
                edge_is_suppressed[edge] = 0
            else:
                edge_is_suppressed[edge] = 1
        return edge_is_suppressed

    def draw(self, vmask, emask):
        edge_is_suppressed = self.suppress_hybridization_edges()
        self.RG.set_edge_filter(edge_is_suppressed)

        pos = gt.sfdp_layout(self.RG, C=4.0, K=1.0)
        if vmask is None and emask is None:
            gt.graph_draw(self.RG, pos=pos,
                          vertex_fill_color=
                          self.get_vertex_property_map('fill_color'),
                          vertex_anchor=1,
                          vertex_text=
                          self.get_vertex_property_map('domain'),
                          vertex_font_size=18,
                          eprops={"color": "black",
                                  "pen_width": 5.5},
                          edge_mid_marker_pos=
                          self.get_edge_property_map(
                              'mid_marker_pos'),
                          edge_mid_marker=
                          self.get_edge_property_map('mid_marker'),
                          edge_end_marker=
                          self.get_edge_property_map('end_marker'),
                          edge_dash_style=
                          self.get_edge_property_map('dash_style'))
        elif emask is None:
            gt.graph_draw(self.RG, pos=pos,
                          vertex_color=vmask,
                          vertex_fill_color=
                          self.get_vertex_property_map('fill_color'),
                          vertex_anchor=1,
                          vertex_text=
                          self.get_vertex_property_map('domain'),
                          vertex_font_size=18,
                          eprops={"color": "black",
                                  "pen_width": 5.5},
                          edge_mid_marker_pos=
                          self.get_edge_property_map(
                              'mid_marker_pos'),
                          edge_mid_marker=
                          self.get_edge_property_map('mid_marker'),
                          edge_end_marker=
                          self.get_edge_property_map('end_marker'),
                          edge_dash_style=
                          self.get_edge_property_map('dash_style'))
        else:
            gt.graph_draw(self.RG, pos=pos,
                          vertex_color=vmask,
                          vertex_fill_color=
                          self.get_vertex_property_map('fill_color'),
                          vertex_anchor=1,
                          vertex_text=
                          self.get_vertex_property_map('domain'),
                          vertex_font_size=18,
                          edge_color=emask,
                          eprops={"pen_width": 5.5},
                          edge_mid_marker_pos=
                          self.get_edge_property_map(
                              'mid_marker_pos'),
                          edge_mid_marker=
                          self.get_edge_property_map('mid_marker'),
                          edge_end_marker=
                          self.get_edge_property_map('end_marker'),
                          edge_dash_style=
                          self.get_edge_property_map('dash_style'))
        self.RG.set_edge_filter(None)
