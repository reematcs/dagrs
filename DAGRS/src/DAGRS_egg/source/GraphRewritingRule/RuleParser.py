from pyparsing import *
import graph_tool.all as gt
import RuleGraph as rg


class RuleParser(object):

    def __init__(self):
        self.rule_criteria = Forward()
        self.rule_mapping = Forward()
        left_bracket = Literal('[').suppress()
        right_bracket = Literal(']').suppress()
        intWord = Word(nums+"-").setParseAction(lambda s, l, t: [int(t[0])])
        realWord = Regex(r"[+-]?\d+\.\d*([eE][+-]?\d+)?").setParseAction(
            lambda s, l, t: [float(t[0])])
        invariant_mapping = Literal("invariant")
        mapping_key = Literal("L") | Literal("R")
        mapping_value_word = (realWord | intWord | Word(alphanums)
                              | dblQuotedString)
        mapping_key_value_pair = Forward()
        mapping_value_string = (mapping_value_word |
                                Group(left_bracket +
                                ZeroOrMore(mapping_key_value_pair) +
                                right_bracket))
        mapping_key_value_pair << Group(mapping_key+mapping_value_string)
        mapping_node = Group(Literal("node") +
                             left_bracket +
                             Group(OneOrMore(mapping_key_value_pair)) +
                             right_bracket)
        self.rule_mapping = Dict(ZeroOrMore(mapping_node) |
                                 invariant_mapping)
        criteria_key = Literal("vertex_1") | Literal("vertex_2") |\
            Literal("property") | Literal("constraint")
        criteria_value_word = (realWord | intWord |
                               Word(alphanums) | dblQuotedString)
        criteria_key_value_pair = Forward()
        criteria_value_string = (criteria_value_word |
                                 Group(left_bracket +
                                 ZeroOrMore(criteria_key_value_pair) +
                                 right_bracket))
        criteria_key_value_pair << Group(criteria_key + criteria_value_string)
        criteria_node = Group(Literal("node") + left_bracket +
                              Group(OneOrMore(criteria_key_value_pair)) +
                              right_bracket)
        criteria_edge = Group(Literal("edge") + left_bracket +
                              Group(OneOrMore(mapping_key_value_pair)) +
                              right_bracket)
        self.rule_criteria = Dict(ZeroOrMore(criteria_node | criteria_edge))

    def wrap(self, tok):
            listtype = type(tok)
            result = {}
            for k, v in tok:
                if type(v) == listtype:
                    result[k] = self.wrap(v)
                else:
                    result[k] = v
            return result

    def parse_rule_criteria(self, file_contents):
        criteria_map = {}
        tokens = self.rule_criteria.parseString(file_contents)
        for t, v in tokens.asList():
            vdict = self.wrap(v)
            v1 = vdict['vertex_1']
            v2 = vdict['vertex_2']
            if v1 not in criteria_map:
                criteria_map = {v1: {}}
            if v2 not in criteria_map[v1]:
                criteria_map[v1][v2] = {vdict['property']: {}}
            criteria_map[v1][v2][vdict['property']] = vdict['constraint']
        return criteria_map

    def rule_criteria_tostring(self, tokens):
        output = ""
        for k, v in tokens.asList():
            output = output+k+" criterion:\n"
            if k == "node":
                vdict = self.wrap(v)
                output = output + "vertex_1\t" +\
                    str(vdict['vertex_1']) + "\t" +\
                    "vertex_2\t" + str(vdict['vertex_2'])+"\n" +\
                    "property\t" + str(vdict['property'])+"\t" +\
                    "constraint\t" + str(vdict['constraint']) + "\n"
            elif k == "edge":
                vdict = self.wrap(v)
                output = output + "source_1\t" + str(vdict['source_1']) +\
                    "target_1\t"+str(vdict['target_1']) + "\n" +\
                    "source_2\t"+str(vdict['source_2']) +\
                    "target_2\t"+str(vdict['target_2']) + "\n" +\
                    "property\t"+str(vdict['property']) +\
                    "constraint\t"+str(vdict['constraint'])
        return output

    def print_rule_criteria(self, tokens):
        print("rule_criteria:")
        print(self.rule_criteria_tostring(tokens))

    def rule_mapping_tostring(self, tokens):
        output = "L\t"+"R\n"
        for k, v in tokens.asList():
                if k == 'node':
                    vdict = self.wrap(v)
                    output = output+str(vdict['L'])+"\t" +\
                        str(vdict['R'])+"\n"
        return output

    def print_rule_mapping(self, tokens):
        print("rule_mapping:")
        print(self.rule_mapping_tostring(tokens))

    def parse_rule_mapping(self, file_contents):

        tokens = self.rule_mapping.parseString(file_contents)
        rule_map = {}
        for t, v in tokens.asList():
            vdict = self.wrap(v)
            v1 = vdict['L']
            v2 = vdict['R']
            if v1 not in rule_map.keys():
                rule_map[v1] = v2
        return rule_map

    def parse_rule_graph(self, rule_name, rule_graph_location):
            rgo = rg.RuleGraph
            vertex_drawing_properties = rgo.vertex_drawing_properties
            edge_drawing_properties = rgo.edge_drawing_properties
            ####Load LHS and RHS of graph rewriting rule from GML file

            vertex_props = rgo.vertex_prop_keys
            edge_props = rgo.edge_prop_keys

            draw_vprops = rgo.draw_vprop_keys
            draw_eprops = rgo.draw_eprop_keys

            rule_graph = rg.RuleGraph(rule_name,
                                      gt.load_graph(rule_graph_location))

            for prop in draw_eprops:
                if prop == 'mid_marker' or prop == 'end_marker':
                    new_string_property =\
                        rule_graph.get_graph().new_edge_property("string")
                    rule_graph.set_edge_property_map(prop, new_string_property)
                elif prop == 'mid_marker_pos':
                    new_float_property =\
                        rule_graph.get_graph().new_edge_property("float")
                    rule_graph.set_edge_property_map(prop, new_float_property)
                elif prop == 'dash_style':
                    new_object_property =\
                        rule_graph.get_graph().new_edge_property("object")
                    rule_graph.set_edge_property_map(prop, new_object_property)
                for e in rule_graph.get_graph().edges():
                    edgebondtype =\
                        rule_graph.get_edge_property_map_value('bond', e)
                    rule_graph.set_edge_property_map_value(
                        prop, e, edge_drawing_properties[edgebondtype][prop])

            for prop in draw_vprops:
                new_string_property =\
                    rule_graph.get_graph().new_vertex_property("string")
                rule_graph.set_vertex_property_map(prop, new_string_property)

            for prop in vertex_props:
                rule_graph.set_vertex_property_map(
                    prop, rule_graph.get_graph().vertex_properties[prop])
                for v in rule_graph.get_graph().vertices():
                    for p in draw_vprops:
                        rule_graph.set_vertex_property_map_value(
                            p, v, vertex_drawing_properties[
                                rule_graph.get_vertex_property_map_value(
                                    'state', v)][p])
            return rule_graph
