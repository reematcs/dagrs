""" A graph rule rewriting map class that maintains existing graph reweriting rules.

.. moduleauthor:: Reem Mokhtar <reem@cs.duke.edu>
"""
import RuleSetup as r
import graph_tool.all as gt
import RuleGraph as rg
import random

class RuleMap(object):
    def __init__(self):
        rs = r.RuleSetup()
        self.rule_names = ['rule_1','rule_2','rule_2_r', 'rule_1_r']
#        self.rule_names = ['rule_2_r', 'rule_1_r']
        self.rule_map = {}
        self.rule_application_path = {}
        for rn in self.rule_names:
            rule = rs.setup_rule_from_folder(rn, home_dir+"/examples/rules/"+rn)
            self.rule_map[rn] = rule
    def apply_rules_random(self, host):
        random.shuffle(self.rule_names)
        for r in self.rule_names:
            changed, subgraph, transformed_host = self.rule_map[r].apply_rule(host, "basic")
            if changed:
                return changed, subgraph, transformed_host
        return False, None, None
    def apply_rule(self, host_state, rule):
        changed, subgraph, transformed_host =\
            self.rule_map[rule].apply_rule(
                host_state.get_graph(), "basic")
        return changed, subgraph, transformed_host        
    def get_rule_names(self):
        return self.rule_names
    def get_rules(self):
        return self.rule_map
    def get_rule(self, rule_name):
        return self.rule_map[rule_name]