import RuleParser as rp
import Rule as rule
import os


class RuleSetup(object):
    def __init__(self):
        self.rule_parser = rp.RuleParser()
        self.criteria_rule = "criteria.rule"
        self.mapping_rule = "mapping.rule"
        self.LHS = "LHS.gml"
        self.RHS = "RHS.gml"
        self.files_per_rule = [self.criteria_rule, self.mapping_rule,
                               self.LHS, self.RHS]

    def setup_rule(self, rule_name, rule_criteria,
                   rule_mapping, LHS_rulegraph, RHS_rulegraph):
        self.parsed_criteria =\
            self.rule_parser.parse_rule_criteria(rule_criteria)
        self.parsed_mapping =\
            self.rule_parser.parse_rule_mapping(rule_mapping)
        self.LHS_rulegraph =\
            self.rule_parser.parse_rule_graph("LHS",
                                              LHS_rulegraph)
        self.RHS_rulegraph =\
            self.rule_parser.parse_rule_graph("RHS",
                                              RHS_rulegraph)
        return rule.Rule(rule_name, self.LHS_rulegraph,
                         self.RHS_rulegraph, self.parsed_mapping,
                         self.parsed_criteria)

    def setup_rule_from_folder(self, rule_name,
                               rule_folder_path="."):
        for file_in_rule in self.files_per_rule:
            file_in_rule_location = os.path.join(rule_folder_path, file_in_rule)
            file_in_rule_handle = open(file_in_rule_location, 'r')
            file_contents = file_in_rule_handle.read()
            if file_in_rule == self.criteria_rule:
                self.parsed_criteria =\
                    self.rule_parser.parse_rule_criteria(file_contents)
            elif file_in_rule == self.mapping_rule:
                self.parsed_mapping =\
                    self.rule_parser.parse_rule_mapping(file_contents)
            elif file_in_rule == self.LHS:
                self.LHS_rulegraph =\
                    self.rule_parser.parse_rule_graph("LHS",
                                                      file_in_rule_location)
            elif file_in_rule == self.RHS:
                self.RHS_rulegraph =\
                    self.rule_parser.parse_rule_graph("RHS",
                                                      file_in_rule_location)
        return rule.Rule(rule_name, self.LHS_rulegraph,
                         self.RHS_rulegraph, self.parsed_mapping,
                         self.parsed_criteria)
