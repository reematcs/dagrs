import RuleSetup as r
import graph_tool.all as gt
import RuleGraph as rg
import RuleMap as rm
import Queue as q
import copy
from subprocess import call
import collections
import RewritingStates as rs
from PyQt5.QtSvg import QSvgRenderer, QGraphicsSvgItem
from PyQt5.QtCore import (QPoint, qAbs, QLineF, QPointF,
                          qrand, QRectF, QSizeF, QSize, qsrand,
                          Qt, QTime)
from PyQt5.QtGui import QImage, QImageWriter, qRgb
from PyQt5.QtGui import (QBrush, QColor, QLinearGradient, QPainter,
                         QPainterPath, QPen, QPolygonF, QRadialGradient)
from PyQt5.QtWidgets import (QApplication, QGraphicsItem, QGraphicsScene,
                             QGraphicsView, QStyle, QDesktopWidget)
import __builtin__

home_dir = "/Users/reemmokhtar/Dropbox/Documents/CS Department" +\
           "/Research/graph_grammars/DNAgrs/code/GraphRewritingSystem" +\
           "/DAGRS/src/DAGRS_egg"

__builtin__.home_dir = home_dir

class RewritingState(QGraphicsItem):
    state_count = 0

    def __init__(self, graphWidget, state_graph, fstate=collections.defaultdict(),
                 frule=[], tstates=collections.defaultdict(),
                 trules=[], state_image_name=""):
        super(RewritingState, self).__init__()
        self.state_image_name = state_image_name
        self.graph = graphWidget
        self.edgeList = []
        self.newPos = QPointF()
        self.setFlag(QGraphicsItem.ItemIsSelectable)        
        self.setFlag(QGraphicsItem.ItemIsMovable)
        self.setCacheMode(QGraphicsItem.DeviceCoordinateCache)        
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges)
        self.setZValue(1)        
        self.from_states = fstate
        self.from_rule = frule
        self.to_states = tstates
        self.to_rules = trules
        self.state_graph = state_graph
        self.name = "state_"+str(self.get_count())
        self.subgraph_queue = q.Queue()

    def __eq__(self, other):
        result = gt.isomorphism(self.state_graph.get_graph(),
                                other.state_graph.get_graph())
        return result

    def get_next_state(self, rule=0, subgraph=""):
        return self.to_states[rule][subgraph]

    def get_next_states(self):
        return self.to_states

    def get_prev_state(self, rule=0, subgraph=""):
        return self.from_states[rule][subgraph]

    def get_prev_states(self):
        return self.from_states

    def add_next_state(self, rule, subgraph, state):
        if rule not in self.to_states.keys():
            self.to_states[rule] = {}
        self.to_states[rule][subgraph] = state

    def add_next_states(self, rules, subgraphs, states):
        for each_rule in rules:
            for subgraph in subgraphs:
                self.to_state[each_rule][subgraph] =\
                    states[each_rule][subgraph]

    def add_prev_state(self, rule, subgraph, state):
        if rule not in self.from_states.keys():
            self.from_states[rule] = {}
        self.from_states[rule][subgraph] = state

    def add_prev_states(self, rules, subgraphs, states):
        for each_rule in rules:
            for subgraph in subgraphs:
                self.from_state[each_rule][subgraph] =\
                    states[each_rule][subgraph]

    def get_state(self):
        return self.state_graph

    def draw_state(self, vmask=None, emask=None):
        self.state_graph.draw(vmask, emask)

    def print_state(self):
        self.state_image_path = self.state_graph.print_graph(self.name)

    def queue_all_matches(self, rule_map):
        for rule_name in rule_map.get_rule_names():
            vmask = None
            emask = None
            rule = rule_map.get_rule(rule_name)
            vertex_mappings, edge_mappings =\
                rule.find_matching_basic(self.state_graph)
            if len(vertex_mappings) == 0:
                continue
            for vm, em in zip(vertex_mappings, edge_mappings):
                vmask, emask =\
                    rule.mark_subgraph_isomorphism(self.state_graph, vm,
                                                   em)
                self.subgraph_queue.put((rule_name, vm, em, vmask, emask))

    def apply_rules(self, rule_map):
        self.queue_all_matches(rule_map)
        while not self.subgraph_queue.empty():
            qitem = self.subgraph_queue.get()
            rule_name, vertex_mapping,\
                edge_mapping, vmask, emask = qitem
            host_state_graph = copy.deepcopy(self.state_graph)
            #self.draw_state(vmask, emask)
            has_changed, subgraph =\
                rule_map.get_rule(rule_name).transform_host(
                    host_state_graph,
                    vertex_mapping, edge_mapping)
            transformed_state = RewritingState(graphWidget=self.graph,
                                               state_graph=host_state_graph)
            self.add_next_state(rule_name, subgraph, transformed_state)
            transformed_state.add_prev_state(rule_name, subgraph, self)
            #transformed_state.draw_state()
            self.state_image_name = transformed_state.print_state()

            #has_changed, subgraph =\
                #rule_map[rule_name].apply_rule(
                    #host_state_copy, "basic", rule_name)
            #changed = changed or has_changed
            #host_state_copy.add_prev_state(
                #rule_name, subgraph, self)
            #self.add_next_state(rule, subgraph, host_state_copy)
        return True

    def get_count(self):
        RewritingState.state_count = RewritingState.state_count+1
        return RewritingState.state_count-1

    def type(self):
        return RewritingState.Type

    def addEdge(self, edge):
        self.edgeList.append(edge)
        edge.adjust()

    def edges(self):
        return self.edgeList

    def calculateForces(self):
        if not self.scene() or self.scene().mouseGrabberItem() is self:
            self.newPos = self.pos()
            return

        # Sum up all forces pushing this item away.
        xvel = 0.0
        yvel = 0.0
        for item in self.scene().items():
            if not isinstance(item, RewritingState):
                continue

        line = QLineF(self.mapFromItem(item, 0, 0), QPointF(0, 0))
        dx = line.dx()
        dy = line.dy()
        l = 2.0 * (dx * dx + dy * dy)
        if l > 0:
            xvel += (dx * 150.0) / l
            yvel += (dy * 150.0) / l

        # Now subtract all forces pulling items together.
        weight = (len(self.edgeList) + 1) * 350.0
        for edge in self.edgeList:
            if edge.sourceNode() is self:
                pos = self.mapFromItem(edge.destNode(), 0, 0)
            else:
                pos = self.mapFromItem(edge.sourceNode(), 0, 0)
        xvel += pos.x() / weight
        yvel += pos.y() / weight

        if qAbs(xvel) < 0.1 and qAbs(yvel) < 0.1:
            xvel = yvel = 0.0

        sceneRect = self.scene().sceneRect()
        self.newPos = self.pos() + QPointF(xvel, yvel)
        self.newPos.setX(min(max(self.newPos.x(),
                                 sceneRect.left() + 100),
                         sceneRect.right() - 100))
        self.newPos.setY(min(max(self.newPos.y(),
                                 sceneRect.top() + 100),
                         sceneRect.bottom() - 100))

    def advance(self):
        if self.newPos == self.pos():
            return False

        self.setPos(self.newPos)
        return True

    def boundingRect(self):
        adjust = 2.0
        return QRectF(-55 - adjust, -55 - adjust, 105 + adjust, 105 + adjust)

    def shape(self):
        path = QPainterPath()
        path.addEllipse(-55, -55, 105, 105)
        return path

    def paint(self, painter, option, widget):
        print("painting")
        painter.drawEllipse(-55, -55, 105, 105)
        print(str(self.name)+"\t(x,y)="+str(self.pos()))
        path = self.state_image_path
        print(path)
        renderer = QSvgRenderer(path)
        loadedImage = QImage(95, 95, QImage.Format_ARGB32_Premultiplied)
        #loadedImage=loadedImage.scaled(180, 180,
            #Qt.KeepAspectRatio, Qt.SmoothTransformation)
        tempPainter = QPainter(loadedImage)
        renderer.render(tempPainter)
        painter.drawImage(QPoint(-45, -45),
                          loadedImage)
        tempPainter.end()
        renderer = QSvgRenderer()
    #painter.drawEllipse(-30, -30, 60, 60)

    def itemChange(self, change, value):
        if change == QGraphicsItem.ItemPositionHasChanged:
            for edge in self.edgeList:
                edge.adjust()
                self.graph.itemMoved()

        return super(RewritingState, self).itemChange(change, value)

    def mousePressEvent(self, event):
        self.update()
        super(RewritingState, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        self.update()
        super(RewritingState, self).mouseReleaseEvent(event)

    def mouseDoubleClickEvent(self, event):
        call(["open", self.state_image_path])            
        super(RewritingState, self).mouseDoubleClickEvent(event)
