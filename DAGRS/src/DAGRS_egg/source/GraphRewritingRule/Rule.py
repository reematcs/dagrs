""" A graph Rule rewriting class.

A graph Rule rewriting class which is a class that represents a graph rewrite
rule conforming to the DAGRS specs
.. moduleauthor:: Reem Mokhtar <reem@cs.duke.edu>
"""
import graph_tool.all as gt
import RuleGraph as rg
import copy


class Rule(object):

    """A Rule rewriting class.

    This class encapsulates the left-hand and right-hand side graphs,
    rule mapping, and vertex criteria.
    """

    def __init__(self, name,
                 left_hand_side, right_hand_side,
                 rule_mapping, vertex_criteria, special_prob_rate_constant=0):
        """Return a Rule object, which represents a graph rewriting rule.

        Parameters
        ----------
        name : string
            A name for the graph rewriting rule.
        left_hand_side : :class:`~GraphRewritingRule.RuleGraph`
            The left-hand side of the graph rewriting rule that
            will be used to determine the rule's applicability.
        right_hand_side : :class:`~GraphRewritingRule.RuleGraph`
            The right-hand side of the graph rewriting rule that
            will be used to transform the subgraph once the rule is
            applied.
        rule_mapping : dict
            A mapping of each vertex between the left_hand_side
            and the right_hand_side of the rule.
        vertex_criteria : dict
            A criteria list for constraints between vertices and
            edges in the left_hand_side graph that should be met
            when matching it to a subgraph in the host graph.
        special_prob_rate_constant : int
            A special probability rate constant that describes
            the kinetic rate of the chemical reaction(s) represented
            by the graph rewriting rule.


        Returns
        -------
        graph_rewriting_rule : :class: `~GraphRewritingRule.Rule`
            Returns a Rule object, which represents a graph
            rewriting rule.

        Notes
        -----


        Examples
        --------

        >>> test_rule_name = "example_rule"
        >>> LHS_rulegraph = rg.RuleGraph("LHS",
        ...     gt.random_graph(3, lambda: (2,2)))
        >>> RHS_rulegraph = rg.RuleGraph("RHS",
        ...     gt.random_graph(3, lambda: (1,1)))
        >>> rule_mapping = [['node', [['L', 0], ['R', 0]]],
        ...                ['node', [['L', 1], ['R', 1]]],
        ...                ['node', [['L', 2], ['R', 2]]]]
        >>> vertex_criteria = {}
        >>> test_rule = Rule(test_rule_name, LHS_rulegraph,
        ...         RHS_rulegraph, rule_mapping, vertex_criteria)
        >>> print(isinstance(test_rule, Rule))
        True
        >>> print(str(test_rule.get_name()) == str(test_rule_name))
        True
        >>> assert test_rule.get_LHS() == LHS_rulegraph
        True
        >>> assert test_rule.get_RHS() == RHS_rulegraph
        True
        >>> print(test_rule.get_all_vertex_criteria() == vertex_criteria)
        True
        >>> print(test_rule.get_rule_mapping() == rule_mapping)
        True
        """
        self.name = ""
        self.left_hand_side = None
        self.right_hand_side = None
        self.RM = {}
        self.vertex_criteria = None
        self.special_prob_rate_constant = 0
        self.set_name(name)
        self.set_LHS(left_hand_side)
        self.set_RHS(right_hand_side)
        self.set_c(special_prob_rate_constant)
        self.set_rule_mapping(rule_mapping)
        self.set_all_vertex_criteria(vertex_criteria)

    def __eq__(self, other):
        """ Compare two Rule objects and return result.

        Returns True if both Rule objects have isomorphic left-hand side graphs,
        right-hand side graphs, equal criteria and mapping.
        """
        if self.get_LHS() == other.get_LHS() and\
            self.get_RHS() == other.get_RHS() and\
            self.get_all_vertex_criteria() ==\
                other.get_all_vertex_criteria() and\
                self.get_rule_mapping() == other.get_rule_mapping():
                return True
        return False

    def get_name(self):
        """ Return the name of a Rule object."""
        return str(self.name)

    def set_name(self, name):
        """ Change the name of a Rule object."""
        self.name = str(name)

    def set_c(self, prob_rate_constant=0):
        """ Change the special probability rate constant"""
        self.c = prob_rate_constant

    def get_LHS(self):
        """ Return left-hand side of graph rewrite rule.

        This method returns the `~GraphRewritingRule.RuleGraph` object that
        represents the left-hand side of the rewrite rule.
        """
        return self.LHS

    def get_RHS(self):
        """ Return right-hand side of graph rewrite rule.

        This method returns the `~GraphRewritingRule.RuleGraph` object that
        represents the left-hand side of the rewrite rule.
        """
        return self.RHS

    def get_c(self):
        """ Return the special probability rate constant."""
        return self.c

    def get_vertex_criterion(self, v1, v2, vertex_property):
        """ Return a specific vertex criterion between two vertices."""
        return self.vertex_criteria[v1][v2][vertex_property]

    def set_vertex_criterion(self, v1, v2, vertex_property, property_value):
        """ Change a specific vertex criterion between two vertices."""
        self.vertex_criteria[v1][v2][vertex_property] = property_value

    def get_vertex_criteria(self, v1, v2):
        """ Return a specific vertex criterion between two vertices."""
        return self.vertex_criteria[v1][v2]

    def get_all_vertex_criteria(self):
        """ Return all vertex criteria."""
        return self.vertex_criteria

    def set_all_vertex_criteria(self, vertex_criteria):
        """ Change all vertex criteria to vertex_criteria."""
        LHS_graph = self.get_LHS().get_graph()
        for v1 in vertex_criteria:
            LHS_v1 = LHS_graph.vertex(v1)
            if self.vertex_criteria is None:
                self.vertex_criteria = {LHS_v1: {}}
            for v2 in vertex_criteria[v1]:
                LHS_v2 = LHS_graph.vertex(v2)
                if self.vertex_criteria[LHS_v1] is None:
                    self.vertex_criteria[LHS_v1] = {LHS_v2: {}}
                self.vertex_criteria[LHS_v1][LHS_v2] =\
                    vertex_criteria[v1][v2]

    def satisfies_vertex_criterion(self, host, v1, v2, vertex_property, vmap):
        """ Check if criteria match between LHS graph and host subgraph."""
        property_value = self.get_vertex_criterion(v1, v2, vertex_property)
        if property_value == "complement" and vertex_property == 'domain':
            domain1 = host.get_vertex_property_map_value(
                vertex_property, vmap[host.get_graph().vertex(v1)])
            domain2 = host.get_vertex_property_map_value(
                vertex_property,
                vmap[host.get_graph().vertex(v2)])
            if (domain1.endswith("*") and domain1[0:-1] == domain2) or \
               (domain2.endswith("*") and domain2[0:-1] == domain1):
                return True
        return False

    def set_rule_mapping(self, rule_mapping):
        """ Change rule mapping between vertices in LHS and RHS graphs."""
        LHS_graph = self.get_LHS().get_graph()
        for v in rule_mapping:
            self.RM[LHS_graph.vertex(v)] = rule_mapping[v]

    def get_rule_mapping(self):
        """ Return rule mapping between vertices in LHS and RHS graphs."""
        return self.RM

    def set_LHS(self, left_hand_side=rg.RuleGraph("LHS")):
        """ Change RuleGraph object of left-hand side."""
        self.LHS = left_hand_side

    def set_RHS(self, right_hand_side=rg.RuleGraph("RHS")):
        """ Change RuleGraph object of right-hand side."""
        self.RHS = right_hand_side

    def apply_rule(self, host, transformation_method):
        """ Find matching host subgraph and apply graph rewriting rule."""
        vertex_mapping = []
        edge_mapping = []
        vmask = []
        emask = []

        if transformation_method == "basic_literal":
            vertex_mapping, edge_mapping =\
                self.find_matching_basic_literal(host)
        elif transformation_method == "basic":
            # vertex_mapping, edge_mapping,
            # vmask, emask =\
            #     self.find_matching_basic(host)
            stuff = self.find_matching_basic(host)
            vertex_mapping, edge_mapping = stuff
            if len(vertex_mapping) == 0:
                return False, None, None
            host_copy = copy.deepcopy(host)
            changed, string_vmap =\
                self.transform_host(host_copy, vertex_mapping,
                                edge_mapping)
           
                
            return changed, string_vmap, host_copy
        return False, None, None
    def find_matching_basic(self, host):
        """ Find all subgraphs isomorphic to LHS in host and return their mapping."""
        vertex_mapping, edge_mapping = \
            self.find_subgraph_isomorphism(host)
        return vertex_mapping, edge_mapping

    def find_matching_basic_literal(self, host):
        """ Find subgraph isomorphic to LHS in host and return a mapping.

            Note: Mapping method here is literal, that is domain names must
            match in both LHS and subgraph, literally.
        """
        LHS_graph = self.LHS.get_graph()
        host_graph = host.get_graph()

        # Find the subgraph isomorphism, and make sure that it
        # matches the domain labels of both the left-hand side
        # graph and the host subgraph.

        vertex_mapping, edge_mapping =\
            self.find_subgraph_isomorphism_literal(host)

        if not len(vertex_mapping) == 0:
            vmask, emask =\
                self.mark_subgraph_isomorphism(host, vertex_mapping,
                                               edge_mapping)

        return vertex_mapping, edge_mapping

    def transform_host(self, host, vertex_mapping,
                       edge_mapping, mapping_choice=0):
        """ Find Subgraph Isomorphism in g_l, resulting in g_l_host.

            g_l_host is a list of the vertices that are
            matched to LHS, and the edges between them
        """
        draw_vprops = rg.RuleGraph.draw_vprop_keys
        draw_eprops = rg.RuleGraph.draw_eprop_keys
        vertex_prop_keys = rg.RuleGraph.vertex_prop_keys
        edge_prop_keys = rg.RuleGraph.edge_prop_keys

        edge_drawing_properties = rg.RuleGraph.edge_drawing_properties
        vertex_drawing_properties = rg.RuleGraph.vertex_drawing_properties
        if not isinstance(vertex_mapping, gt.PropertyMap):
            tvm = vertex_mapping[len(vertex_mapping)-1]
            tem = edge_mapping[len(edge_mapping)-1]
        else:
            tvm = vertex_mapping
            tem = edge_mapping

        # Apply all vertex properties in rule RHS to the vertices
        # in the matching host subgraph mapping (tvm)

        for eachvm in self.get_RHS().get_graph().vertices():
            for each_prop in draw_vprops:
                host.set_vertex_property_map_value(
                    each_prop, host.get_graph().vertex(tvm[eachvm]),
                    self.get_RHS().get_vertex_property_map_value(
                        each_prop, self.get_RHS().get_graph().vertex(eachvm)))
            for each_prop in vertex_prop_keys:
                if each_prop == "domain":
                    continue
                host.set_vertex_property_map_value(
                    each_prop, host.get_graph().vertex(tvm[eachvm]),
                    self.get_RHS().get_vertex_property_map_value(
                        each_prop, self.get_RHS().get_graph().vertex(eachvm)))
        # Perform destroyed edge check on g_l_host (edge missing in RHS,
        # exists in LHS)
        changed = self.destroy_edges(host, tvm, tem)

        # Perform created edge check on g_l_host (edge missing in LHS,
        # exists in RHS)
        changed = self.create_edges(
            host, tvm, tem) or changed

        pos1 = gt.sfdp_layout(host.get_graph(),
                              C=4.0, K=1.0)
        if changed:
            vmask, emask = gt.mark_subgraph(host.get_graph(),
                                                        self.get_LHS().get_graph(),
                                                        tvm,
                                                        tem)
            changed = False

        string_vmap = ""
        for lhs_v in self.LHS.get_graph().vertices():
            string_vmap = string_vmap + str(tvm[lhs_v])
        return changed, string_vmap
            
    def create_edges(self, host, tvm, tem):
        changed = False
        edge_drawing_properties = rg.RuleGraph.edge_drawing_properties
        edge_drawing_property_keys = rg.RuleGraph.draw_eprop_keys
        RHS = self.get_RHS()
        LHS = self.get_LHS()
        RHS_graph = RHS.get_graph()
        LHS_graph = LHS.get_graph()

        host_graph = host.get_graph()
        for eachem in RHS_graph.edges():
            mappingsource = eachem.source()
            mappingtarget = eachem.target()
            if RHS_graph.edge(mappingsource, mappingtarget) \
                and not LHS_graph.edge(
                    self.RM[mappingsource],
                    self.RM[mappingtarget])\
                and not host_graph.edge(tvm[mappingsource],
                                        tvm[mappingtarget]):                
                changed = True
                host_graph.add_edge(
                    tvm[mappingsource],
                    tvm[mappingtarget])
                if RHS.get_edge_property_map_value(
                "bond", RHS_graph.edge(
                    mappingsource,
                    mappingtarget)) == "hybridization":
                    host_graph.add_edge(
                        tvm[mappingtarget],
                        tvm[mappingsource])
                host.set_edge_property_map_value(
                    'bond',
                    host_graph.edge(
                        tvm[mappingsource],
                        tvm[mappingtarget]),
                    RHS.get_edge_property_map_value(
                        'bond',
                        RHS_graph.edge(
                            mappingsource,
                            mappingtarget)))
                for each_prop in edge_drawing_property_keys:
                    host.set_edge_property_map_value(
                        each_prop,
                        host_graph.edge(
                            tvm[mappingsource],
                            tvm[mappingtarget]),
                        edge_drawing_properties['hybridization'][each_prop])
                    host.set_edge_property_map_value(
                        each_prop,
                        host_graph.edge(tvm[mappingtarget],
                                        tvm[mappingsource]),
                        edge_drawing_properties['hybridization'][each_prop])                    
                if 'bond' not in host.get_edge_property_maps().keys():
                    host.set_edge_property_map(
                        'bond',
                        RHS_graph.new_edge_property("string"))

        return changed

    def destroy_edges(self, host, tvm, tem):
        changed = False
        edge_drawing_properties = rg.RuleGraph.edge_drawing_properties
        edge_drawing_property_keys = rg.RuleGraph.draw_eprop_keys
        RHS = self.get_RHS()
        LHS = self.get_LHS()
        RHS_graph = RHS.get_graph()
        LHS_graph = LHS.get_graph()
        host_graph = host.get_graph()
        
        for eachem in LHS_graph.edges():
            mappingsource = eachem.source()
            mappingtarget = eachem.target()
            if LHS_graph.edge(mappingsource, mappingtarget) \
                and not RHS_graph.edge(self.RM[mappingsource],
                                       self.RM[mappingtarget])\
                and host_graph.edge(tvm[mappingsource],
                                    tvm[mappingtarget]):
                changed = True
                host_graph.remove_edge(
                    host_graph.edge(tvm[mappingsource],
                                    tvm[mappingtarget]))
                if host_graph.edge(
                    tvm[mappingtarget],
                    tvm[mappingsource]) and\
                   LHS.get_edge_property_map_value(
                    "bond", LHS_graph.edge(
                        mappingsource, mappingtarget))\
                == "hybridization":
                    host_graph.remove_edge(
                        host_graph.edge(tvm[mappingtarget],
                                        tvm[mappingsource]))                
        return changed

    def mark_subgraph_isomorphism(self, host,
                                  vertex_mapping, edge_mapping):
        vmask = None
        emask = None
        LHS_graph = self.LHS.get_graph()
        host_graph = host.get_graph()

        host.get_graph().set_vertex_filter(None)
        host.get_graph().set_edge_filter(None)
        vmask, emask = gt.mark_subgraph(host_graph,
                                        LHS_graph,
                                        vertex_mapping,
                                        edge_mapping)

        host.get_graph().set_vertex_filter(vmask)
        host.get_graph().set_edge_filter(emask)
        assert(gt.isomorphism(host.get_graph(), LHS_graph))

        host.get_graph().set_vertex_filter(None)
        host.get_graph().set_edge_filter(None)

        return vmask, emask

    def find_subgraph_isomorphism(self, host):
        LHS = self.get_LHS()
        LHS_graph = LHS.get_graph()
        host_graph = host.get_graph()
        if LHS_graph.num_edges() == 0 or \
           host_graph.num_edges() == 0:
            elabel = None
        else:
            elabel = (LHS.get_edge_property_map('bond'),
                      host.get_edge_property_map('bond'))
        # Find the subgraph isomorphism, and make sure that it
        # matches the state labels of both the left-hand side
        # graph and the host subgraph.
        vertex_mapping_state, edge_mapping_state = \
            gt.subgraph_isomorphism(LHS_graph,
                                    host.get_graph(), vertex_label=
                                    (LHS.get_vertex_property_map('state'),
                                    host.get_vertex_property_map('state')),
                                    edge_label=elabel, random=True)


        edge_mapping = []
        vmscount = 0
        vertex_mapping = []
        if len(vertex_mapping_state) == 0:
            return vertex_mapping, edge_mapping
        satisfied = True
        for vms in vertex_mapping_state:
            vcriteria = self.get_all_vertex_criteria()
            for v1 in vcriteria:
                for v2 in vcriteria[v1]:
                    for prop_name in vcriteria[v1][v2]:
                        satisfied =\
                            self.satisfies_vertex_criterion(host, v1,
                                                            v2,
                                                            prop_name, vms)
                        if not satisfied:
                            break
            if satisfied:
                vertex_mapping.append(vms)
                edge_mapping.append(edge_mapping_state[vmscount])
            vmscount = vmscount + 1


        return vertex_mapping, edge_mapping

    def find_subgraph_isomorphism_literal(self, host):
        LHS = self.get_LHS()
        LHS_graph = LHS.get_graph()
        host_graph = host.get_graph()
        if LHS_graph.num_edges() == 0 or \
           host_graph.num_edges() == 0:
            elabel = None
        else:
            elabel = (LHS.get_edge_property_map('bond'),
                      host.get_edge_property_map('bond'))
        # Find the subgraph isomorphism, and make sure that it
        # matches the domain labels of both the left-hand side
        # graph and the host subgraph.
        vertex_mapping_domain, edge_mapping_domain = \
            gt.subgraph_isomorphism(LHS_graph,
                                    host_graph, vertex_label=
                                    (LHS.get_vertex_property_map('domain'),
                                     host.get_vertex_property_map('domain')),
                                    edge_label=elabel, random=True)
        vertex_mapping_state, edge_mapping_state = \
            gt.subgraph_isomorphism(LHS_graph,
                                    host_graph, vertex_label=
                                    (LHS.get_vertex_property_map('state'),
                                    host.get_vertex_property_map('state')),
                                    edge_label=elabel, random=True)


        vertex_mapping, edge_mapping =\
            self.find_subgraph_isomorphism_mapping_intersection(
                vertex_mapping_domain, edge_mapping_domain,
                vertex_mapping_state, edge_mapping_state)
        return vertex_mapping, edge_mapping

    def find_subgraph_isomorphism_mapping_intersection(
            self, vmap1, emap1, vmap2, emap2):
        vmscount = 0

        matrix_mapping = [[0 for x in xrange(len(vmap1))]
                          for x in xrange(len(vmap2))]
        for vm1 in vmap1:
            vmdcount = 0
            for vm2 in vmap2:
                for v in self.get_LHS().get_graph().vertices():
                    if str(vm1[v]) != str(vm2[v]):
                        matrix_mapping[vmdcount][vmscount] = False
                        break
                    else:
                        matrix_mapping[vmdcount][vmscount] = True
                vmdcount = vmdcount+1
            vmscount = vmscount+1

        edge_mapping = []
        vmscount = 0
        vertex_mapping = []

        for vm1 in vmap1:
            vmdcount = 0
            for vm2 in vmap2:
                if matrix_mapping[vmdcount][vmscount]:
                    vertex_mapping.append(vm2)
                    edge_mapping.append(emap2[vmdcount])
                vmdcount = vmdcount+1
            vmscount = vmscount+1
        return vertex_mapping, edge_mapping
