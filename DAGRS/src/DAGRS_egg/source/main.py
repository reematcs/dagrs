import graph_tool.all as gt
import GraphRewritingRule.RuleSetup as r
import GraphRewritingRule.RuleGraph as rg
import GraphRewritingRule.RuleMap as rm
import GraphRewritingRule.RewritingStates as rss
import GraphRewritingRule.RewritingState as rs
import copy
import math
from PyQt5.QtSvg import QSvgRenderer, QGraphicsSvgItem
from PyQt5.QtCore import (QPoint, qAbs, QLineF, QPointF,
                          qrand, QRectF, QSizeF, QSize, qsrand,
                          Qt, QTime)
from PyQt5.QtGui import QImage, QImageWriter, qRgb
from PyQt5.QtGui import (QBrush, QColor, QLinearGradient, QPainter,
                         QPainterPath, QPen, QPolygonF, QRadialGradient)
from PyQt5.QtWidgets import (QApplication, QGraphicsItem, QGraphicsScene,
                             QGraphicsView, QStyle, QDesktopWidget)
import __builtin__

home_dir = "/Users/reemmokhtar/Dropbox/Documents/CS Department" +\
           "/Research/graph_grammars/DNAgrs/code/GraphRewritingSystem" +\
           "/DAGRS/src/DAGRS_egg"

__builtin__.home_dir = home_dir


class GraphWidget(QGraphicsView):
    def __init__(self):
        super(GraphWidget, self).__init__()
        scene = QGraphicsScene(self)
        scene.setItemIndexMethod(QGraphicsScene.NoIndex)
        screen = QDesktopWidget().screenGeometry()

        scene.setSceneRect(-screen.width(), -screen.height(),
                           screen.width(), screen.height())
        self.setScene(scene)
        self.setCacheMode(QGraphicsView.CacheBackground)
        self.setViewportUpdateMode(QGraphicsView.BoundingRectViewportUpdate)
        self.setRenderHint(QPainter.Antialiasing)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorViewCenter)
        
        home_dir = "/Users/reemmokhtar/Dropbox/Documents/" +\
                       "CS Department/Research/" +\
                       "graph_grammars/DNAgrs/code/GraphRewritingSystem/DAGRS/src/" +\
                       "DAGRS_egg"
        __builtin__.home_dir = home_dir
        
        self.timerId = 0
        
        copy_mem = {}
                
        host_graph_1 = rg.RuleGraph(
            "host_graph_1", gt.load_graph(
                home_dir + "/examples/input_species/example4.gml"))
        
        state_0 = rs.RewritingState(graphWidget=self,
                                    state_graph=copy.deepcopy(host_graph_1,
                                                              copy_mem)) 
                                   
    
        rewriting_states = rss.RewritingStates(graphwidget=self,
                                               state_0=state_0)
    
        state_0.print_state()
        rewriting_states.explore_all_states()
        rewriting_states.draw_all_states(scene)
        
        #host_graph = host_graph_1
        #state_0.apply_rules(rule_map)
        #state_0.print_state()
        #rewriting_states.explore_all_states()

        #node1 = Node("state1", self)
        #node2 = Node("state2", self)
        #node3 = Node("state3", self)
        #scene.addItem(node1)
        #scene.addItem(node2)
        #scene.addItem(node3)
        #scene.addItem(Edge(node1, node2))
        #scene.addItem(Edge(node2, node3))

        #node1.setPos(-850, -450)
        #node2.setPos(-650, -450)
        #node3.setPos(-450, -450)

        self.setWindowTitle("Reaction Graph/States")        
    
        #while True:
            #changed, subgraph, transformed_host =\
                #rule_map.apply_rules_random(host_graph)
            #if not changed:
                    #break
            #pos1 = gt.sfdp_layout(
                #transformed_host.get_graph(), C=4.0, K=1.0)
    
            #to_continue = raw_input('Continue?[y/n]:')
            #if to_continue == "n":
                #print("Done")
                #break
            #elif to_continue == "y":
                #continue
            #else:
                #print("Did not understand input. Exiting.")
                #sys.exit(0)
        #host_graph = transformed_host
    
        #if not changed:
            #print("No more rules to apply.")
        #sys.exit(app.exec_())

    def itemMoved(self):
        if not self.timerId:
            self.timerId = self.startTimer(1000 / 25)

    def keyPressEvent(self, event):
        key = event.key()

        if key == Qt.Key_Plus:
            self.scaleView(1.2)
        elif key == Qt.Key_Minus:
            self.scaleView(1 / 1.2)
        elif key == Qt.Key_Space or key == Qt.Key_Enter:
            for item in self.scene().items():
                if isinstance(item, rs.RewritingState):
                    item.setPos(-150 + qrand() % 300, -150 + qrand() % 300)
                else:
                    super(GraphWidget, self).keyPressEvent(event)

    def timerEvent(self, event):
        nodes = [item
                 for item in self.scene().items() if isinstance(item, rs.RewritingState)]

        for node in nodes:
            node.calculateForces()

        itemsMoved = False
        for node in nodes:
            if node.advance():
                itemsMoved = True

        if not itemsMoved:
            self.killTimer(self.timerId)
            self.timerId = 0

    def wheelEvent(self, event):
        self.scaleView(math.pow(2.0, -event.angleDelta().y() / 240.0))

    def drawBackground(self, painter, rect):
        # Shadow.
        sceneRect = self.sceneRect()
        rightShadow = QRectF(sceneRect.right(), sceneRect.top() + 5, 5,
                             sceneRect.height())
        bottomShadow = QRectF(sceneRect.left() + 5, sceneRect.bottom(),
                              sceneRect.width(), 5)
        if rightShadow.intersects(rect) or rightShadow.contains(rect):
            painter.fillRect(rightShadow, Qt.darkGray)
        if bottomShadow.intersects(rect) or bottomShadow.contains(rect):
            painter.fillRect(bottomShadow, Qt.darkGray)

        # Fill.
        gradient = QLinearGradient(sceneRect.topLeft(),
                                   sceneRect.bottomRight())
        gradient.setColorAt(0, Qt.white)
        gradient.setColorAt(1, Qt.lightGray)
        painter.fillRect(rect.intersected(sceneRect), QBrush(gradient))
        painter.setBrush(Qt.NoBrush)
        painter.drawRect(sceneRect)

        # Text.
        # textRect = QRectF(sceneRect.left() + 4, sceneRect.top() + 4,
        #                   sceneRect.width() - 4, sceneRect.height() - 4)
        # message = "Click and drag the nodes around, and zoom with the " \
        #           "mouse wheel or the '+' and '-' keys"

        # font = painter.font()
        # font.setBold(True)
        # font.setPointSize(14)
        # painter.setFont(font)
        # painter.setPen(Qt.lightGray)
        # painter.drawText(textRect.translated(2, 2), message)
        # painter.setPen(Qt.black)
        # painter.drawText(textRect, message)

    def scaleView(self, scaleFactor):
        factor = self.transform().scale(
            scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width()

        if factor < 0.07 or factor > 100:
            return

        self.scale(scaleFactor, scaleFactor)

if __name__ == '__main__':

    import sys

    app = QApplication(sys.argv)
    qsrand(QTime(0, 0, 0).secsTo(QTime.currentTime()))

    widget = GraphWidget()
    widget.show()
    sys.exit(app.exec_())

