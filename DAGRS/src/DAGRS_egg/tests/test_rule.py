import unittest
from ..source.GraphRewritingRule import RuleSetup as r
from ..source.GraphRewritingRule import RuleGraph as rg
import graph_tool.all as gt


class Test_Rule(unittest.TestCase):
    def setUp(self):
        self.rule_name = "rule_1"
        self.test_rule_path = "./source/rules/rule_1"
        self.rs = r.RuleSetup()
        self.test_rule =\
            self.rs.setup_rule_from_folder(self.rule_name,
                                           self.test_rule_path)
        self.test_graph = rg.RuleGraph(
            self.rule_name, gt.load_graph(self.test_rule_path+"/LHS.gml"))
        self.test_graph2 = rg.RuleGraph(
            self.rule_name, gt.load_graph(self.test_rule_path+"/RHS.gml"))
        self.test_mapping = [['node', [['L', 0], ['R', 0]]],
                             ['node', [['L', 1], ['R', 1]]]]

    def test_get_name(self):
        assert self.test_rule.get_name() == self.rule_name

    def test_set_name(self):
        new_name = "rule_2"
        self.test_rule.set_name(new_name)
        assert self.test_rule.get_name() == new_name
        self.test_rule.set_name(self.rule_name)

    def test_get_LHS(self):
        assert self.test_graph == self.test_rule.get_LHS()

    def test_get_RHS(self):
        assert self.test_graph2 == self.test_rule.get_RHS()

    def test_not_get_LHS(self):
        assert self.test_graph != self.test_rule.get_RHS()

    def test_not_get_RHS(self):
        print(self.test_graph)
        assert self.test_graph2 != self.test_rule.get_LHS()

    def test_get_c(self):
        assert self.test_rule.get_c() == 0

    def test_get_rule_mapping(self):
        assert self.test_rule.get_rule_mapping() == self.test_mapping

    def test_set_rule_mapping(self):
        old_test_mapping = self.test_rule.get_rule_mapping()
        test_mapping = [['node', [['L', 1], ['R', 0]]],
                        ['node', [['L', 0], ['R', 1]]]]
        self.test_rule.set_rule_mapping(test_mapping)
        assert self.test_rule.get_rule_mapping() != old_test_mapping

if __name__ == '__main__':
    unittest.main()
