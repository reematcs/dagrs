graph [
   directed 1
   node [
      id 0
      domain "d1"
      state "unhybridized"
   ]
   node [
      id 1
      domain "d1*"
      state "unhybridized"
   ]
   node [
      id 2
      domain "d1"
      state "unhybridized"
   ]
   node [
      id 3
      domain "d1*"
      state "unhybridized"
   ]
   node [
      id 4
      domain "d1"
      state "unhybridized"
   ]
   node [
      id 5
      domain "d1*"
      state "unhybridized"
   ]
   node [
      id 6
      domain "d1"
      state "unhybridized"
   ]
   node [
      id 7
      domain "d1*"
      state "unhybridized"
   ]
   node [
      id 8
      domain "d1"
      state "unhybridized"
   ]
   node [
      id 9
      domain "d1*"
      state "unhybridized"
   ]
]
