graph [
   directed 1
   node [
      id 0
      domain "d1"
      state "hybridized"
   ]
   node [
      id 1
      domain "d2"
      state "hybridized"
   ]
   node [
      id 2
      domain "d2*"
      state "hybridized"
   ]
   node [
      id 3
      domain "d2*"
      state "unhybridized"
   ]
   node [
      id 4
      domain "d1*"
      state "hybridized"
   ]
   edge [
      id 0
      source 0
      target 1
      bond "covalent"
   ]
   edge [
      id 1
      source 3
      target 4
      bond "covalent"
   ]
   edge [
      id 2
      source 1
      target 2
      bond "hybridization"
   ]
   edge [
      id 3
      source 0
      target 4
      bond "hybridization"
   ]
   edge [
      id 4
      source 2
      target 1
      bond "hybridization"
   ]
   edge [
      id 5
      source 4
      target 0
      bond "hybridization"
   ]
]
