graph [
   directed 1
   node [
      id 0
      domain "d1"
      state "hybridized"
   ]
   node [
      id 1
      domain "d1*"
      state "hybridized"
   ]
   edge [
      id 0
      source 0
      target 1
      bond "hybridization"
   ]
   edge [
      id 1
      source 1
      target 0
      bond "hybridization"
   ]
  
]
